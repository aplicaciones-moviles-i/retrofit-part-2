package facci.pm.volleyapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RequestQueue requestQueue;

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recycleViewUsers);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        requestQueue = Volley.newRequestQueue(this);

        CargarDatosDesdeServidor();

    }

    public void CargarDatosDesdeServidor(){
        String URL = "https://jsonplaceholder.typicode.com/users";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, URL, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //textView.setText(response.toString());
                List<User> listaUsuarios = new ArrayList<User>();
                for (int indice = 0; indice < response.length(); indice++) {

                    try {
                        Log.e("JSON OBJECT", response.get(indice).toString());
                        JSONObject jsonObject = new JSONObject(response.get(indice).toString());
                        listaUsuarios.add(new User(jsonObject.getInt("id"), jsonObject.getString("name"), jsonObject.getString("username")));
                        UserListAdapter userListAdapter = new UserListAdapter(listaUsuarios);
                        recyclerView.setAdapter(userListAdapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
    requestQueue.add(jsonArrayRequest);
    }


}